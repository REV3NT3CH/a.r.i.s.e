##                                               ##
##  sonophilos tweak.prop  ##
##      Lover of Sound          ##
##     XDA Developers       ##
##                                       ##
##    Read comments!  ##
BACKUP=y

#### Begin ViPERAudio™ / ARISE™ suggested props ####

## Audio Offload Tweaks #
# Identical to those included in guitardedhero default
audio.offload.gapless.enabled=true
audio.offload.multiaac.enable=true
audio.offload.passthrough=false
audio.offload.video=false
audio.offload.pcm.enable=true
#audio.offload.pcm.16bit.enable=true
audio.offload.pcm.24bit.enable=true
audio.offload.pcm.32bit.enable=true

## HTC™ / Dolby™ Audio properties #
# Identical to those included in guitardedhero default
# MAY HAVE NO EFFECT EXCEPT ON HTC WITH SENSE
persist.htc.audio.pcm.channels=2.1
# System props for Dolby
dmid=-1286820014
audio.ds1.metainfo.key=273
# DS2 works on RC8, on 8.x no aux volume, RC9/9.1 work
audio.dolby.ds2.enabled=true
audio.dolby.ds2.hardbypass=true
dolby.audio.sink.info=speaker
# Should be equivalent to the HTC10 Atmos GUI settings
# These are where my settings differ from GH's slightly
dolby.ds.dialogenhancer.state=on
dolby.ds.graphiceq.state=off
dolby.ds.hpvirtualizer.state=off
dolby.ds.intelligenteq.preset=Flat
dolby.ds.intelligenteq.state=on
dolby.ds.profile.name=Music
dolby.ds.spkvirtualizer.state=on
dolby.ds.state=on
dolby.ds.volumeleveler.state=off

## HTC™ Effect switch
# Uncomment ONLY one to choose effect
# BEFORE flashing tweakprop.zip (default Dolby)
htc.audio.effect.dolby=1
#htc.audio.effect.harman=-1
#htc.audio.effect.jbl=4

## Huawei™ Audio #
#
ro.config.hw_dts=true
ro.config.hw_music_lp=true
ro.config.music_lp_vol=true

## Lenovo™ K4 Note Dolby™ Audio ##
# SEPARATED into Extras, enable if using "new" Atmos GUI
#lenovo-sw heww3 add for dolby support
#LENOVO_DOLBY_SUPPORT = yes
#lenovo-sw heww3 add end
#ro.mtk_dolby_dap_support=1
#lenovo-sw heww3 add for hifi support begin
#ro.lenovo.hifi_support=true
#ro.lenovo.soundeffect=dolby
#lenovo-sw heww3 add for hifi support end
#

## LG™ G5 Audio #
# REMOVED voice quality tweaks, were causing mic issues
ro.config.media_vol_extend=ON

## LG™ V10 Audio #
#
mm.enable.smoothstreaming=true
ro.lge.audio_soundexception=true

## Meizu™ Audio #
#
persist.audio.dirac.speaker=true

## Sony™ Audio #
#
ro.semc.sound_effects_enabled=true
ro.semc.enhance.supported=true
ro.somc.clearphase.supported=true
ro.semc.clearaudio.supported=true
ro.semc.xloud.supported=true
persist.service.enhance.enable=1
persist.service.clearphase.enable=1
persist.service.clearaudio.enable=1
persist.service.xloud.enable=1
com.sonymobile.clearphase_enabled=true
com.sonyericsson.xloud_enabled=true
ro.somc.cp.default_setting=true
ro.somc.xloud.default_setting=true
# LDAC audio support
ro.somc.ldac.audio.supported=true
# Force LDAC to run in Bluetooth
persist.service.ldac.enable=1
com.sonymobile.ldac_enabled=true
# Enable downsampling for multichannel content >44100Khz
# May conflict with resampler, only relevant with multichannel content (ie 5.1 encoded audio or video)
#audio.playback.mch.downsample=true

## SONY effect #
sony.support.effect=0x1FF
#  Allocate bit for each effect.
#  If necessary, define effect list with logical sum.
#  EFFECT_DN        0x0001 (Dynamic Normalizer)
#  EFFECT_SF        0x0002 (S-Force)
#  EFFECT_CPHP      0x0004 (ClearPhase Headphone)
#  EFFECT_CA        0x0008 (ClearAudio)
#  EFFECT_VPT       0x0010 (VPT)
#  EFFECT_CPSP      0x0020 (ClearPhase Speaker)
#  EFFECT_XLOUD     0x0040 (xLOUD)
#  EFFECT_CAPLUS    0x0080 (ClearAudio+)
#  EFFECT_SPBUNDLE  0x0100 (Speaker Bundle)
#  EFFECT_ALC       0x0200 (ALC)

# Effect used by Speaker Bundle.
# EFFECT_SF | EFFECT_CPSP | EFFECT_SPBUNDLE = 0x122
sony.effect.custom.sp_bundle=0x122

# Effect used by ClearAudio+ headset
# EFFECT_CA | EFFECT_VPT | EFFECT_CAPLUS | EFFECT_ALC = 0x298
sony.effect.custom.caplus_hs=0x298

# Effect used by ClearAudio+ speaker
# EFFECT_CA | EFFECT_VPT | EFFECT_CPSP | EFFECT_CAPLUS | EFFECT_ALC = 0x2B8
sony.effect.custom.caplus_sp=0x2B8
## End Sony™ Effect ##

## ViPER|Audio™ Miscellaneous #
# AwesomePlayer disabler - may be necessary for v4a
media.stagefright.use-awesome=false
persist.dev.pm.dyn_samplingrate=1
qcom.hw.aac.encoder=true
tunnel.audio.encode=false
tunnel.audiovideo.decode=false
tunnel.decode=false
lpa.decode=false
lpa.releaselock=false
lpa.use-stagefright=false
mmp.enable.3g2=true
media.aac_51_output_enabled=true

## Vivo™ DTS™ #
#
use.dts_m6=true
use.dts_m6_notify=true
use.dts_eagle=true

#### End ViPERAudio / ARISE suggested props ####

#### Begin Sonophilos experimental props ####
## Thoroughly tested, all positive - none detract ##
# Legacy AT tweaks - these reduce signal noise noticeably
# Thanks Inspire Android for this, wherever you found it
htc.audio.alt.enable=0
htc.audio.hac.enable=0

# Sonophilos added props vs Guitardedhero default
audio.offload.multiple.enabled=true
# Enable ONLY on ARM64 devices
#audio.offload.pcm.64bit.enable=true
## Audio Offload Buffer #
# Set this value higher in powers of 2 if you experience cracking, popping or skipping even with Ondemand gov.
audio.offload.buffer.size.kb=128
# 256, 512, 1024 are also acceptable values for above
# Lower will generally be lower latency - 128 is M9 ideal
#
# Gain = perceived volume, but higher is not always better
# Can go as high as 14512 - this is a smooth value for M9
htc.audio.swalt.mingain=1920
persist.htc.audio.pcm.samplerate=192000
audio_hal.period_size=192
htc.audio.lpa.a2dp=0
htc.audio.alc.enable=1
persist.audio.SupportHTCHWAEC=1
persist.audio.vr.enable=false
ro.config.hw_dolby=true
support_boomsound_effect=true
support_harman=true
mpq.audio.decode=true
persist.sys.media.use-awesome=0
# af.resampler.quality REMOVED, obsolete in MM+
audio.converter.samplerate.preferred=libsamplerate
audio.converter.samplerate.libsamplerate.quality=best
clock.allow_streaming_errors=false
default.pcm.rate_converter=samplerate_best
persist.audio.hp=true
persist.audio.lowlatency.rec=false
persist.hwc.ptor.enable=true
persist.speaker.prot.enable=false
tunnel.multiple=true
sys.keep_app_1=com.vipercn.viper4android_v2

## smeejaytee plundered effect props #
#
# enable pbe effects
audio.safx.pbe.enabled=true
# property for AudioSphere Post processing
audio.pp.asphere.enabled=true
dolby.monospeaker=off
htc.audio.beats.support2v=1
htc.audio.effect.srs=1
htc.audio.beats.state=1
htc.audio.beats.config=1
htc.audio.effectendpoint=2
htc.audio.hdaudio.videoplayback=1

# Experimental Meizu lines from smeejaytee
ro.mtk_audio_ape_support=1
ro.mtk_flv_playback_support=1
ro.mtk_wmv_playback_support=1
ro.mtk_audenh_support=1
ro.mtk_bessurround_support=1
ro.mtk_bt_support=1
ro.mtk_audio_tuning_tool_ver=V2.2
ro.have_aacencode_feature=1
