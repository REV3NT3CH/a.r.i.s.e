##                          ##
#    ARISE Sound Systems™    #
#                            #
#       Miscellaneous        #
##                          ##

#
The Miscellaneous folder is the catch-all for everything else provided that can be used with the sound system.
#

#
The DTSClientControllerService apk can be installed to either /system/app or as a /data or user installation by tapping the apk after extracting it from the zip. Some users may experience Force Closes after installing, however, it still remains to be seen if this app has any effect once installed anyway.
#

#
The flashable Android N Permissive.zip is strongly recommended to be flashed immediately after the sound system if an Android N user is flashing the sound system for the first time on an Android N system. This zip prevents the forced Safe Mode reboot caused by the sound system's /system/su.d/50viper.sh by the following result after flashing it:

- /system/su.d/permissive.sh
- deletion of /system/bin/logd
#

#
The ARISE Sound Systems™ Audio Calibration Database flashable zip is for experimental and testing purposes only. Issues are certainly expected until a proper combination is found for a particular device. Commented tweak.prop entries are provided to persist each file and combinations of different device files are certainly possible. Support for this component is very limited as devices react differently to audio calibration database data.
#
